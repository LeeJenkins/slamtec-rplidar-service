#/*
# * Copyright (C) 2014  RoboPeak
# * Copyright (C) 2014 - 2018 Shanghai Slamtec Co., Ltd.
# *
# * This program is free software: you can redistribute it and/or modify
# * it under the terms of the GNU General Public License as published by
# * the Free Software Foundation, either version 3 of the License, or
# * (at your option) any later version.
# *
# * This program is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# *
# * You should have received a copy of the GNU General Public License
# * along with this program.  If not, see <http://www.gnu.org/licenses/>.
# *
# */
#

###
### this file was copied and modified from the slamtec SDK
###

## the SDK uses the variable HOME_TREE
HOME_TREE := ~/rplidar_sdk

MODULE_NAME := slamtec_rplidar_service

## $(info ### in Makefile, $$HOME_TREE is [${HOME_TREE}])

C_INCLUDES += -I$(HOME_TREE)/sdk/include -I$(HOME_TREE)/sdk/src
LD_LIBS += -lstdc++ -lpthread

export
include ./mak_def.inc

CXXSRC += slamtec_rplidar_service.cpp

EXTRA_OBJ :=

all: build_app
	cp $(APP_TARGET) .

include ./mak_common.inc

clean: clean_app
